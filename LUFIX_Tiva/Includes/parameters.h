#ifndef INCLUDES_PARAMETERS_H_
#define INCLUDES_PARAMETERS_H_

#include <limits.h>
#include "tm4c123gh6pm.h"

/* ---------------------------- Application  ----------------------------------*/

#define NUMREG_DAC      (4)

/* ------------------------- SSI Configuration --------------------------------*/

#define SSI_BITRATE      (1000000) //1MHz
#define SSI_FRAMESIZE    (16)

/* ----------------------- Modbus Configuration -------------------------------*/

#define MB_STELLARIS_DEBUG      (0)
#define MB_MODE                 (MB_RTU)
#define MB_SLAVEID              (0x01)
#define MB_PORT                 (0)
#define MB_BAUDRATE             (19200)
#define MB_PARITY               (MB_PAR_EVEN)

/* --------------------------- Modbus LED -------------------------------------*/

#define MB_LED_PERIPHERAL       (SYSCTL_PERIPH_GPIOB)
#define MB_LED_PORT             (GPIO_PORTB_BASE)
#define MB_LED_PIN              (GPIO_PIN_7)

/* --------------------------- Modbus UART ------------------------------------*/

#if (MB_STELLARIS_DEBUG)

   #define MB_UART_MODULE       (UART0_BASE)
   #define MB_UART_INT          (INT_UART0)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART0)
   #define MB_UART_GPIO_PORT    (GPIO_PORTA_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_0)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_1)
   #define MB_UART_GPIO_RX      (GPIO_PA0_U0RX)
   #define MB_UART_GPIO_TX      (GPIO_PA1_U0TX)

#else

   #define MB_UART_MODULE       (UART3_BASE)
   #define MB_UART_INT          (INT_UART3)
   #define MB_UART_PERIPH       (SYSCTL_PERIPH_UART3)
   #define MB_UART_GPIO_PORT    (GPIO_PORTC_BASE)
   #define MB_UART_GPIO_PIN_RX  (GPIO_PIN_6)
   #define MB_UART_GPIO_PIN_TX  (GPIO_PIN_7)
   #define MB_UART_GPIO_RX      (GPIO_PC6_U3RX)
   #define MB_UART_GPIO_TX      (GPIO_PC7_U3TX)

#endif

/* ------------------------- Modbus Registers ---------------------------------*/

#define COILS_START               (1)      //coils starting address
#define COILS_NUM                 (0)      //number of coils

#define DISCRETES_START           (10001)  //discretes starting addres
#define DISCRETES_NUM             (0)      //number of discrete inputs

#define INPUT_REGISTERS_START     (30001)  //input registers starting address
#define INPUT_REGISTERS_NUM       (0)      //number of input registers

#define HOLDING_REGISTERS_START   (40001)  //holding registers starting address
#define HOLDING_REGISTERS_NUM     (12)     //number of holding registers

#define REG_NUM_BYTES             (2)      //number of bytes in register
#define REG_ADDR_OFFSET           (1)      //bit/register address offset

/* ------------------- Modbus Register Configuration --------------------------*/

//HOLDING REGISTERS
#define HOLDING_DAC_START    (1)
#define HOLDING_DAC_END      (8)
#define HOLDING_FF_START     (9)
#define HOLDING_FF_END       (12)

/* --------------------- Modbus Callback Functions ----------------------------*/

#define MBCB_COILS_DEFAULT      (1)
#define MBCB_DISCRETE_DEFAULT   (1)
#define MBCB_INPUTREG_DEFAULT   (1)
#define MBCB_HOLDING_DEFAULT    (0)

/* ------------------------- Modbus Global Registers --------------------------*/

UCHAR  ucMBCoils[(COILS_NUM + (CHAR_BIT - 1)) / CHAR_BIT];            //Coils
UCHAR  ucMBDiscretes[(DISCRETES_NUM + (CHAR_BIT - 1)) / CHAR_BIT];    //Discrete Inputs
USHORT usMBInputReg[INPUT_REGISTERS_NUM];                             //Input Registers
USHORT usMBHoldingReg[HOLDING_REGISTERS_NUM];                         //Holding Registers

/*------------------------ GPIO Pin Configuration -----------------------------*/

/*
 * PA5 - SSI0 Tx (DAC)
 * PA2 - SSI0 Clk (DAC)
 *
 * PB5 - GPIO Output (DAC1 SYNC)
 * PB0 - GPIO Output (DAC2 SYNC)
 *
 * PC6 - UART Rx (Modbus)
 * PC7 - UART Tx (Modbus)
 * PB7 - GPIO Output (Modbus LED)
 *
 * PD0 - GPIO Output (Flip-Flop Data Bit 0)
 * PD1 - GPIO Output (Flip-Flop Data Bit 1)
 * PD2 - GPIO Output (Flip-Flop Data Bit 2)
 * PD3 - GPIO Output (Flip-Flop Data Bit 3)
 *
 * PE0 - GPIO Output (Flip-Flop 1 CLK)
 * PE1 - GPIO Output (Flip-Flop 2 CLK)
 * PE2 - GPIO Output (Flip-Flop 3 CLK)
 * PE3 - GPIO Output (Flip-Flop 4 CLK)
 *
 */

#endif //INCLUDES_PARAMETERS_H_
