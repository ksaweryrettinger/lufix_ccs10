// LUFIX - TM4C123GH6PM
// Ksawery Wieczorkowski-Rettinger <kwrettinger@gmail.com>

/* ----------------------- Includes -----------------------------------------------*/

#include <FreeMODBUS/include/mb.h>
#include <FreeMODBUS/mbport/include/port.h>
#include <FreeMODBUS/include/mbutils.h>
#include "parameters.h"

int main(void)
{
    /*------------------------ System Clock Configuration (50MHz) -----------------------*/

    SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);

    /*-------------------------------- GPIO Peripherals ---------------------------------*/

    //Enable GPIO peripherals
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA); //DAC SSI
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB); //Modbus LED, DACs
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOC); //UART (Modbus)
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD); //Flip-Flops
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE); //Flip-Flops

    /*---------------------------------- GPIO Outputs -----------------------------------*/

    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_5); //SSI Slave Select
    GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    GPIOPinTypeGPIOOutput(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

    /*---------------------------------- GPIO Initialization ----------------------------*/

    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_5, GPIO_PIN_0 | GPIO_PIN_5);      //PB0, PB5 = 1
    GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);  //PD0, PD1, PD2, PD3 = 0
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);  //PE0, PE1, PE2, PE3 = 0

    /*-------------------------------- SSI Configuration --------------------------------*/

    SysCtlPeripheralEnable(SYSCTL_PERIPH_SSI0);
    while(!SysCtlPeripheralReady(SYSCTL_PERIPH_SSI0)) {};
    GPIOPinConfigure(GPIO_PA2_SSI0CLK); //CLK
    GPIOPinConfigure(GPIO_PA5_SSI0TX);  //MOSI
    GPIOPinTypeSSI(GPIO_PORTA_BASE, GPIO_PIN_2 | GPIO_PIN_5);
    SSIConfigSetExpClk(SSI0_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_2, //CPOL = 1, CPHA = 0
                       SSI_MODE_MASTER, SSI_BITRATE, SSI_FRAMESIZE);     //1Mhz, 16-bits
    SSIEnable(SSI0_BASE);

    SysCtlDelay((SysCtlClockGet()/3/1000000) * 100); //100us delay

    /*----------------------------- Flip-Flop Initialization ----------------------------*/

    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3,
                 GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);
    GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);

    /*-------------------------------- DAC Initialization ----------------------------*/

    //DAC #1
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0); //PB5 = 0
    SSIDataPut(SSI0_BASE,  0x2000);
    while (SSIBusy(SSI0_BASE)) {}
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5); //PB5 = 1

    //DAC #2
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, 0); //PB0 = 0
    SSIDataPut(SSI0_BASE, 0x2000);
    while (SSIBusy(SSI0_BASE)) {}
    GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, GPIO_PIN_0); //PB0 = 1

    /*------------------------ MODBUS Protocol Stack Initialization ---------------------*/

    //Note: Modbus GPIO and UART configured in parameters.h
    (void) eMBInit(MB_MODE, MB_SLAVEID, MB_PORT, MB_BAUDRATE, MB_PARITY);
    (void) eMBEnable();

    while (1)
    {
        /*--------------------------------- Modbus ------------------------------------------*/

        (void) eMBPoll();
    }
}

/*------------------------------------ MODBUS Holding Register Callback ------------------------------------------*/

eMBErrorCode eMBRegHoldingCB(uint8_t * pucRegBuffer, uint16_t usAddress, uint16_t usNRegs, eMBRegisterMode eMode)
{
    eMBErrorCode eStatus = MB_ENOERR;
    uint16_t usRegIndex;
    uint16_t usRegTemp;
    uint16_t usFrameDAC;

    usAddress -= 1;

    if ((usAddress >= HOLDING_REGISTERS_START) && (usAddress + usNRegs <= HOLDING_REGISTERS_START + HOLDING_REGISTERS_NUM))
    {
        usRegIndex = (uint16_t) (usAddress - HOLDING_REGISTERS_START);
        switch (eMode)
        {
            case MB_REG_READ:

                while (usNRegs > 0)
                {
                    *pucRegBuffer++ = (uint8_t)(usMBHoldingReg[usRegIndex] >> 8);
                    *pucRegBuffer++ = (uint8_t)(usMBHoldingReg[usRegIndex] & 0xFF);
                    usRegIndex++;
                    usNRegs--;
                }
                break;

            case MB_REG_WRITE:

                while (usNRegs > 0)
                {
                    usRegTemp = *pucRegBuffer++ << 8;
                    usRegTemp |= *pucRegBuffer++;
                    usMBHoldingReg[usRegIndex] = usRegTemp;

                    if (usRegIndex <= (HOLDING_DAC_END - 1))
                    {
                        /*---------------------------------- DAC Write ----------------------------------*/

                        if (usRegIndex < NUMREG_DAC) //DAC #1
                        {
                            //Create DAC frame
                            usFrameDAC = (0x1000 | (usRegIndex << 14)); //address and write mode
                            usFrameDAC |= (usRegTemp & 0x0FFF); //data (12-bits)

                            //Send frame to DAC
                            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, 0); //PB5 = 0
                            SSIDataPut(SSI0_BASE, usFrameDAC);
                            while (SSIBusy(SSI0_BASE)) {}
                            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_5, GPIO_PIN_5); //PB5 = 1
                        }
                        else //DAC #2
                        {
                            //Create DAC frame
                            usFrameDAC = (0x1000 | ((usRegIndex - 4) << 14)); //address and write mode
                            usFrameDAC |= (usRegTemp & 0x0FFF); //data (12-bits)

                            //Send frame to DAC
                            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, 0); //PB0 = 0
                            SSIDataPut(SSI0_BASE, usFrameDAC);
                            while (SSIBusy(SSI0_BASE)) {}
                            GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_0, GPIO_PIN_0); //PB0 = 1
                        }

                        /*-------------------------------------------------------------------------------*/
                    }
                    else if (usRegIndex <= (HOLDING_FF_END - 1))
                    {
                        /*------------------------------- Flip-Flop Write -------------------------------*/

                        //Prepare data (4 LSB)
                        GPIOPinWrite(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, usRegTemp & 0x000F);

                        //Clock flip-flop
                        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, usRegIndex - (HOLDING_FF_START - 1));
                        GPIOPinWrite(GPIO_PORTE_BASE, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3, 0);

                        /*-------------------------------------------------------------------------------*/
                    }

                    usRegIndex++;
                    usNRegs--;
                }
                break;
        }
    }
    else
    {
        eStatus = MB_ENOREG;
    }

    return eStatus;
}
